import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;
import java.util.Scanner;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author TI
 */
public class Desenho extends JFrame {

    public Desenho(JPanel panel) {
        JFrame frame= new JFrame("Desenho do maior Triangulo em cm");
        frame.getContentPane().add(panel);

        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setSize(300, 300);
        frame.setVisible(true);
        frame.setAlwaysOnTop(true);
    }
    static class Triangulos extends JPanel {

        Point p1;
        Point p2;
        Point p3;

        public Triangulos(Point ponto1, Point ponto2, Point ponto3) {
            this.p1 = ponto1;
            this.p2 = ponto2;
            this.p3 = ponto3;
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.setColor(Color.BLACK);
            Polygon poligono = new Polygon();
            poligono.addPoint(p1.x, p1.y);
            poligono.addPoint(p2.x, p2.y);
            poligono.addPoint(p3.x, p3.y);
            g.drawPolygon(poligono);
        }
    }
}