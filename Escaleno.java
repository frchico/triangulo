/**
 * Created by 20161lbsi120268 on 13/06/2017.
 */
public class Escaleno extends Triangulo {
    private String cor;
    public Escaleno(int ladoA, int ladoB, int ladoC) {
        setLadoA(ladoA);
        setLadoB(ladoB);
        setLadoC(ladoC);
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getCor() {
        return cor;

    }

}
