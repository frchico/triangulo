/**
 * Created by 20161lbsi120268 on 13/06/2017.
 */
public class Equilatero extends Triangulo {

    public Equilatero(int ladoA, int ladoB, int ladoC) {
        setLadoA(ladoA);
        setLadoB(ladoB);
        setLadoC(ladoC);
    }

}
