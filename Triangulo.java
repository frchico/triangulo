/**
 * Created by 20161lbsi120268 on 13/06/2017.
 */
public abstract class Triangulo {
    private int LadoA = -1;
    private int LadoB = 0;
    private int LadoC = 0;

    public int getLadoA() {
        return LadoA;
    }

    public void setLadoA(int ladoA) {
        LadoA = ladoA;
    }

    public int getLadoB() {
        return LadoB;
    }

    public void setLadoB(int ladoB) {
        LadoB = ladoB;
    }

    public int getLadoC() {
        return LadoC;
    }

    public void setLadoC(int ladoC) {
        LadoC = ladoC;
    }
}



