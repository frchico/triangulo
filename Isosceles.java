/**
 * Created by 20161lbsi120268 on 13/06/2017.
 */
public class Isosceles extends Triangulo {
    private String nome;
    public Isosceles(int ladoA, int ladoB, int ladoC) {
        setLadoA(ladoA);
        setLadoB(ladoB);
        setLadoC(ladoC);
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNome(){
        return this.nome;
    }
}
