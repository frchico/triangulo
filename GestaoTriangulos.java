import javax.sound.midi.Soundbank;
import java.awt.*;
import java.util.Scanner;

/**
 * Created by 20161lbsi120268 on 13/06/2017.
 */
public class GestaoTriangulos {
    private Triangulo[] listaTriangulos = new Triangulo[100];
    private int indice = 0;
    private boolean fim;

    public Triangulo[] getListaTriangulos() {
        return listaTriangulos;
    }

    public int getIndice() {
        return indice;
    }

    public boolean isFim() {
        return fim;
    }

    public void encerrar(boolean fim) {
        this.fim = fim;
    }
    public boolean getFim(){
        return fim;
    }

    public boolean cadastrarTriangulo() {
        boolean auxiliar = false;
        Scanner input = new Scanner(System.in);
        int ladoA;
        int ladoB;
        int ladoC;
        System.out.print("Informe o lado A(em metros): ");
        ladoA = input.nextInt();
        if (ladoA == 0) {
            encerrar(false);
            return false;
        } else {
            encerrar(true);

        }
        System.out.print("Informe o lado B(em metros): ");
        ladoB = input.nextInt();
        System.out.print("Informe o lado C(em metros): ");
        ladoC = input.nextInt();
        if (ladoA <= 0 || ladoB <= 0 || ladoC <= 0) {
            System.out.println("Não é um triângulo");
            return false;
        } else if (ladoA == ladoB && ladoC == ladoA) {
            listaTriangulos[indice] = new Equilatero(ladoA, ladoB, ladoC);
        } else if (ladoA == ladoB || ladoA == ladoC || ladoB == ladoC) {
            Isosceles i = new Isosceles(ladoA, ladoB, ladoC);
            System.out.print("Deseja definir um nome? ");
            auxiliar = input.nextBoolean();
            if (auxiliar) {
                System.out.print("Informe o nome: ");
                input.nextLine();
                String aux = input.nextLine();
                i.setNome(aux);
            }
            listaTriangulos[indice] = i;
        } else {
            System.out.print("Deseja definir a cor? ");
            auxiliar = input.nextBoolean();
            Escaleno e = new Escaleno(ladoA, ladoB, ladoC);
            if (auxiliar) {
                System.out.print("Informe a cor: ");
                input.nextLine();
                String aux = input.nextLine();
                e.setCor(aux);
            }
            listaTriangulos[indice] = e;
        }
        indice++;
        return true;
    }
    public void menu (){
        int i = 0;
        do {
            if (cadastrarTriangulo()) {
                i++;
            }
        } while (getFim());
    }
    public void qtdTotal (){
        System.out.println("O total de triangulos digitados é: " + getIndice());
    }
    public void qtdPorTipo (){
        int qtdEq = 0;
        int qtdIs = 0;
        int qtdEs = 0;
        for (int i = 0; i <= indice; i++){
            if(listaTriangulos[i] instanceof Equilatero){
                qtdEq++;
            }
            else if(listaTriangulos[i] instanceof Isosceles){
                qtdIs++;
            }
            else if(listaTriangulos[i] instanceof Escaleno) {
                qtdEs++;
            }
        }
        System.out.println("Equilatero: " + qtdEq);
        System.out.println("Isosceles: " + qtdIs);
        System.out.println("Escaleno: " + qtdEs);
    }
    public float obterAreaEq (Equilatero t1){
        return (float) ((t1.getLadoA()*Math.sqrt(3))/4);
    }
    public void areaEquilateros (){
        boolean existe = false;
        for(int i = 0; i <= indice; i++){
            if(listaTriangulos[i] instanceof Equilatero){
                existe = true;
                System.out.println("Area dos triangulos equilateros: ");
                break;
            }
        }
        int maior = 0;
        if(existe) {
            for (int i = 0; i <= indice; i++) {
                if (listaTriangulos[i] instanceof Equilatero) {
                    if (listaTriangulos[i].getLadoA() > maior) {
                        maior = listaTriangulos[i].getLadoA();
                    }
                    System.out.println("lado: " + listaTriangulos[i].getLadoA() + " area: " + obterAreaEq((Equilatero) listaTriangulos[i]));
                }
            }
            imprimirMaiorTriangulo(maior);
        }

    }
    public void imprimirMaiorTriangulo (int lado){
        lado = lado *100;
        new Desenho(new Desenho.Triangulos(new Point(100, 100), new Point(100, 100+lado), new Point(100+lado, 100)));
    }

    public static void main(String[] args) {
        GestaoTriangulos g = new GestaoTriangulos();
        g.menu();
        g.qtdTotal();
        g.qtdPorTipo();
        g.areaEquilateros();

    }


}
